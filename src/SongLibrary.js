import {getLibrary} from "./common/Utils";
import {ListGroup, Form} from "react-bootstrap";
import ListItem from "./ListItem";
import React from "react";

class SongLibrary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filteredSongs: getLibrary()
        }
    }

    filterSongs(value) {
        this.setState({
            filteredSongs: getLibrary().filter(element => {
                return element.title.toLowerCase().includes(value.toLowerCase());
            })
        });
    }

    render() {
        return (
            <div className="library">
                <Form.Group>
                    <Form.Control size="lg" type="text" placeholder="Search" onKeyUp={(e) => {
                        this.filterSongs(e.target.value)
                    }}/>
                </Form.Group>
                <ListGroup>
                    {
                        this.state.filteredSongs.map((song) => <ListItem song={song}/>)
                    }
                </ListGroup>
            </div>
        );
    }
}

export default SongLibrary;
