import React from 'react';
import './App.css';
import Sidebar from "./Sidebar";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Song from "./Song";
import SongContextProvider from "./common/SongContext";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <SongContextProvider>
                        <Sidebar/>
                        <div className="scope full-height">
                            <Switch>
                                <Route path="/:alias" children={<Song/>}/>
                            </Switch>
                        </div>
                    </SongContextProvider>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
