import React from 'react';
import {Accordion, Card, Button, ListGroup} from "react-bootstrap";
import {getPlaylist} from "./common/Utils";
import ListItem from "./ListItem";

class Playlist extends React.Component {
    render() {
        const playlist = getPlaylist();
        return (
            <Accordion>
                {
                    playlist.map((playlistItem) => (
                        <Card>
                            <Card.Header>
                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    {playlistItem.title}
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <ListGroup variant={"flush"}>
                                        {
                                            playlistItem.songs.map((song) => {
                                                if (song.song.title) {
                                                    return <ListItem song={song.song} comment={song.comment} key={song.alias}/>;
                                                } else {
                                                    return <small>{song.song}</small>
                                                }
                                            })
                                        }
                                    </ListGroup>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    ))
                }
            </Accordion>
        );
    }
}

export default Playlist;
