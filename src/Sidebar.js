import React from "react";
import SongLibrary from "./SongLibrary";
import Logo from "./Logo";
import {Tab, Tabs} from "react-bootstrap";
import Playlist from "./Playlist";

class Sidebar extends React.Component {
    render() {

        return (
            <div className="sidebar">
                <Logo/>
                <Tabs defaultActiveKey="playlist">
                    <Tab eventKey="library" title="Library">
                        <SongLibrary/>
                    </Tab>
                    <Tab eventKey="playlist" title="Playlist">
                        <Playlist/>
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default Sidebar;
