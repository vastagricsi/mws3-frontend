import React, {Component, createContext} from "react";

export const SongContext = createContext('');


class SongContextProvider extends Component {
    state = {
        currentAlias: ''
    }
    render() {
        return (
            <SongContext.Provider value={{...this.state}}>
                {this.props.children}
            </SongContext.Provider>
        );
    }
}

export default SongContextProvider;
