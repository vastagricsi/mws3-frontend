import songData from "../songs.json";
import playlistData from "../playlists.json";

export function getLibrary() {
    return songData;
}

export function getPlaylist() {
    let playlist = playlistData;
    playlist.forEach((item, i) => {
        item.songs.forEach((playlistItem, k) => {
            let song = getSong(playlistItem.song);
            if (song.title) {
                playlist[i].songs[k].song = song;
            }
        });
    });
    return playlist;
}

export function getSong(alias) {
    let chosenSong = {};
    songData.forEach((song) => {
        if (song.alias === alias) {
            chosenSong = song;
        }
    });
    return chosenSong;
}
