import {ListGroup} from "react-bootstrap";
import {Link} from "react-router-dom";
import {SongContext} from "./common/SongContext";
import {Component} from "react";

class ListItem extends Component {
    static  contextType = SongContext;

    constructor(props) {
        super(props);
        this.state = {
            currentAlias: ''
        };
    }

    setSong(song) {
        this.setState({currentAlias: song.alias});
    }

    render() {
        let song = this.props.song;
        let comment = this.props.comment;


        return (
            <ListGroup.Item key={song.alias} active={this.state.currentAlias === song.alias}>
                <Link onClick={() => {this.setSong(song)}} to={`/${song.alias}`}>
                    {song.title}
                    <small> ({comment})</small>
                </Link>
            </ListGroup.Item>
        );
    }
}

export default ListItem;
