import React from 'react';
import {getSong} from "./common/Utils";
import {SongContext} from "./common/SongContext";
import {withRouter} from "react-router";

class Song extends React.Component {

    static contextType = SongContext;

    render() {
        let alias = this.props.location.pathname.replace('/', '');
        let song = getSong(alias);
        this.context.currentAlias = alias;
        if (!song.src) {
            console.log(song);
            return <h1>Song not found</h1>
        }
        if (song.src.includes('jpg')) {
            return <img title={song.title} width="100%" src={song.src}/>
        } else {
            return <iframe width="100%" className="full-height" src={song.src}/>
        }
    }
}

export default withRouter(Song);
